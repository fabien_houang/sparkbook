import com.amazonaws.services.kinesis.model.GetRecordsRequest
import com.amazonaws.services.kinesis.model.Record
import scala.collection.JavaConversions._

import org.apache.spark.{SparkContext, SparkConf}

import org.apache.spark.rdd._

import org.joda.time.DateTime
import Converter._

object Joda {
    implicit def dateTimeOrdering: Ordering[DateTime] =
        Ordering.fromLessThan(_ isBefore _)
}

import Joda._

object Action {

    import KinesisProducer.kinesisClient

    val conf = new SparkConf()
        .setAppName("SparkBook")
        .setMaster("local[*]")

    val sc = SparkContext.getOrCreate(conf)


    def getUserFromStream() : RDD[User] = {

        val stream = "streamUser"
        val shardIds = kinesisClient
                        .describeStream(stream)
                        .getStreamDescription()
                        .getShards()
                        .map(_.getShardId())
        val getRecordRequest = new GetRecordsRequest()
        val list = shardIds
            .flatMap(shardId => {
                    val shardIt = kinesisClient
                                    .getShardIterator(stream, shardId,
                                                      "TRIM_HORIZON")
                                    .getShardIterator()
                    getRecordRequest.setLimit(10000)

                    def getFromNext(shardIter: String,
                                    list: List[List[Record]]) : List[User] = {
                        getRecordRequest.setShardIterator(shardIter)
                        val getRecResult = kinesisClient
                                             .getRecords(getRecordRequest)

                        val rec = getRecResult
                                    .getRecords()
                                    .toList

                        val next = getRecResult.getNextShardIterator()
                        rec.length match {
                            case i if i > 0 => getFromNext(next, rec :: list)
                            case 0 => (rec :: list)
                                            .flatten
                                            .map(rec => new String(rec.getData()
                                                                      .array(),
                                                                      "utf-8"))
                                            .flatMap(StringToUser)
                        }
                    }
                    getFromNext(shardIt, List())
            })

        sc.parallelize(list.toSeq)
    }

    def getPostFromStream() : RDD[Post] = {

        val stream = "streamPost"
        val shardIds = kinesisClient
                        .describeStream(stream)
                        .getStreamDescription()
                        .getShards()
                        .map(_.getShardId())
        val getRecordRequest = new GetRecordsRequest()
        val list = shardIds
            .flatMap(shardId => {
                    val shardIt = kinesisClient
                                    .getShardIterator(stream, shardId,
                                                      "TRIM_HORIZON")
                                    .getShardIterator()
                    getRecordRequest.setLimit(10000)

                    def getFromNext(shardIter: String,
                                    list: List[List[Record]]) : List[Post] = {
                        getRecordRequest.setShardIterator(shardIter)
                        val getRecResult = kinesisClient
                                             .getRecords(getRecordRequest)

                        val rec = getRecResult
                                    .getRecords()
                                    .toList

                        val next = getRecResult.getNextShardIterator()
                        rec.length match {
                            case i if i > 0 => getFromNext(next, rec :: list)
                            case 0 => (rec :: list)
                                            .flatten
                                            .map(rec => new String(rec.getData()
                                                                      .array(),
                                                                      "utf-8"))
                                            .flatMap(StringToPost)
                        }
                    }
                    getFromNext(shardIt, List())
            })

        sc.parallelize(list.toSeq)
    }

    def getMessageFromStream() : RDD[Message] = {

        val stream = "streamMessage"
        val shardIds = kinesisClient
                        .describeStream(stream)
                        .getStreamDescription()
                        .getShards()
                        .map(_.getShardId())
        val getRecordRequest = new GetRecordsRequest()
        val list = shardIds
            .flatMap(shardId => {
                    val shardIt = kinesisClient
                                    .getShardIterator(stream, shardId,
                                                      "TRIM_HORIZON")
                                    .getShardIterator()
                    getRecordRequest.setLimit(10000)

                    def getFromNext(shardIter: String,
                                    list: List[List[Record]]) : List[Message] = {
                        getRecordRequest.setShardIterator(shardIter)
                        val getRecResult = kinesisClient
                                             .getRecords(getRecordRequest)

                        val rec = getRecResult
                                    .getRecords()
                                    .toList

                        val next = getRecResult.getNextShardIterator()
                        rec.length match {
                            case i if i > 0 => getFromNext(next, rec :: list)
                            case 0 => (rec :: list)
                                            .flatten
                                            .map(rec => new String(rec.getData()
                                                                      .array(),
                                                                      "utf-8"))
                                            .flatMap(StringToMessage)
                        }
                    }
                    getFromNext(shardIt, List())
            })

        sc.parallelize(list.toSeq)
    }

    def getUserInfo(name: String) : Option[User] =
        getUserFromStream().filter(_.name == name) match {
            case rdd if rdd.isEmpty() => None
            case rdd => Some(rdd.first)
        }

    def getPostsFromUser(user: String) =
        getPostFromStream()
            .filter(_.author == user)
    def get10MostRecentPosts() =
        getPostFromStream()
            .takeOrdered(10)(Ordering[DateTime]
                                .reverse
                                .on(_.postedOn))

    def getMessageFromUser(user: String) =
        getMessageFromStream()
            .filter(_.author == user)
    def getMessageSentTo(user: String) =
        getMessageFromStream()
            .filter(_.target == user)
    def getMessageFromTo(user: String, other: String) =
        getMessageFromStream()
            .filter(m => m.author == user && m.target == other ||
                         m.target == user && m.author == other)

    def getBrandOccurrences(from: DateTime, to: DateTime, brands: List[String]) = {
        val occ_post = LoadFromHDFS.s3loadPost
                        .filter(p => p.postedOn.isBefore(to) && p.postedOn.isAfter(from))
                        .map(_.text)
                        .map(line => line.split(' '))
                        .filter(lst => brands
                            .forall(brand => lst.exists(word => word.equalsIgnoreCase(brand))))
                        .count

        val occ_msg = LoadFromHDFS.s3loadMessage
                        .filter(msg => msg.postedOn.isBefore(to) && msg.postedOn.isAfter(from))
                        .map(_.text)
                        .map(line => line.split(' '))
                        .filter(lst => brands
                            .forall(brand => lst
                                .exists(word => word.equalsIgnoreCase(brand))))
                        .count

        occ_post + occ_msg
    }

    def closeSC() = sc.stop()
}
