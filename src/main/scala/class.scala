import org.joda.time.DateTime

case class User(name: String, age: Int, city: String)
case class Post(author: String, postedOn: DateTime, text: String)
case class Message(author: String, target: String,
                   postedOn: DateTime, text:String)
