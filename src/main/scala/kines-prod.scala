import java.nio.ByteBuffer

import scala.util.Random

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.services.kinesis.AmazonKinesisClient
import com.amazonaws.services.kinesis.model.PutRecordRequest
import org.apache.log4j.{Level, Logger}

import org.apache.spark.SparkConf
import org.apache.spark.internal.Logging
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Milliseconds, StreamingContext}
import org.apache.spark.streaming.dstream.DStream.toPairDStreamFunctions
import org.apache.spark.streaming.kinesis.KinesisInitialPositions.Latest
import org.apache.spark.streaming.kinesis.KinesisInputDStream

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD

import play.api.libs.json._



object KinesisProducer {

  implicit val userWrite = Json.writes[User]
  implicit val dateWrites = Writes.jodaDateWrites("yyyy-MM-dd HH::mm")
  implicit val postWrite = Json.writes[Post]
  implicit val messageWrite = Json.writes[Message]

  val kinesisClient = new AmazonKinesisClient(new DefaultAWSCredentialsProviderChain())
  val endpoint = "https://kinesis.eu-west-3.amazonaws.com"
  kinesisClient.setEndpoint(endpoint)
  
  // Set default log4j logging level to WARN to hide Spark logs
  StreamingExamples.setStreamingLogLevels()

  def sendUser(data: User) = {
    val stream = "streamUser"
    val putRecordRequest = new PutRecordRequest()
    putRecordRequest.setStreamName(stream)
    putRecordRequest.setData(ByteBuffer.wrap(Json.stringify(Json.toJson(data)).getBytes))
    putRecordRequest.setPartitionKey(s"User-PK-${data.name}")
    val putRecordResult = kinesisClient.putRecord(putRecordRequest)
  }

  def sendPost(data: Post) = {
    val stream = "streamPost"
    val putRecordRequest = new PutRecordRequest()
    putRecordRequest.setStreamName(stream)
    putRecordRequest.setData(ByteBuffer.wrap(Json.stringify(Json.toJson(data)).getBytes))
    putRecordRequest.setPartitionKey(s"Post-PK-${data.author}")
    val putRecordResult = kinesisClient.putRecord(putRecordRequest)
  }

  def sendMessage(data: Message) = {
    val stream = "streamMessage"
    val putRecordRequest = new PutRecordRequest()
    putRecordRequest.setStreamName(stream)
    putRecordRequest.setData(ByteBuffer.wrap(Json.stringify(Json.toJson(data)).getBytes))
    putRecordRequest.setPartitionKey(s"Message-PK-${data.author}")
    val putRecordResult = kinesisClient.putRecord(putRecordRequest)
  }
}
