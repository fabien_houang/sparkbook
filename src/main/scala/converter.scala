import play.api.libs.json._

object Converter {
    implicit val userFormat = Json.format[User]
    implicit val dateReads = Reads.jodaDateReads("yyyy-MM-dd HH::mm")
    implicit val dateWrites = Writes.jodaDateWrites("yyyy-MM-dd HH::mm")
    implicit val postFormat = Json.format[Post]
    implicit val messageFormat = Json.format[Message]



    def StringToUser(str: String) = Json.parse(str).validate[User]
        match {
            case JsError(e) => println(e); None
            case JsSuccess(t, _) => Some(t)
        }
    def StringToPost(str: String) = Json.parse(str).validate[Post]
        match {
            case JsError(e) => println(e); None
            case JsSuccess(t, _) => Some(t)
        }
    def StringToMessage(str: String) = Json.parse(str).validate[Message]
        match {
            case JsError(e) => println(e); None
            case JsSuccess(t, _) => Some(t)
        }
}
