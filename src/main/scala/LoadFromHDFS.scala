import awscala._
import s3._
import Converter._

import org.apache.spark.{SparkContext, SparkConf}

import org.apache.spark.rdd._

object LoadFromHDFS {
  
  val conf = new SparkConf()
    .setAppName("SparkBook")
    .setMaster("local[*]")
  val sc = SparkContext.getOrCreate(conf)

  def s3loadUser : RDD[User] = {
    val name = "User"
    val bucketname = "sparkbooks3"
    implicit val s3 = S3.at(Region.Paris)
    val bucket = s3.bucket(bucketname).get
    val summaries = bucket.objectSummaries.toList
    val files = summaries.filter(o => { name == o.getKey.split("/")(0) })
                          .map(o => o.getKey )
                          .sorted
    val alljsons = (0 to files.length-1).map( i => {
      val recent = files(i)
      val s3obj = s3.getObject("sparkbooks3", recent)
      val jsons = scala.io.Source.fromInputStream(s3obj.getObjectContent())
                                                       .mkString.split("}")
                                                       .map(json => json + "}")
      jsons.flatMap(StringToUser)
    })

    val json = alljsons.flatten.toSeq
    val rdd = sc.parallelize(json)
    rdd.foreach(println)
    rdd
  }

  def s3loadMessage : RDD[Message] = {
    val name = "User"
    val bucketname = "sparkbooks3"
    implicit val s3 = S3.at(Region.Paris)
    val bucket = s3.bucket(bucketname).get
    val summaries = bucket.objectSummaries.toList
    val files = summaries.filter(o => { name == o.getKey.split("/")(0) })
      .map(o => o.getKey )
      .sorted
    val alljsons = (0 to files.length-1).map( i => {
      val recent = files(i)
      val s3obj = s3.getObject("sparkbooks3", recent)
      val jsons = scala.io.Source.fromInputStream(s3obj.getObjectContent())
        .mkString.split("}")
        .map(json => json + "}")
      jsons.flatMap(StringToMessage)
    })

    val json = alljsons.flatten.toSeq
    val rdd = sc.parallelize(json)
    rdd.foreach(println)
    rdd
  }

  def s3loadPost : RDD[Post] = {
    val name = "Post"
    val bucketname = "sparkbooks3"
    implicit val s3 = S3.at(Region.Paris)
    val bucket = s3.bucket(bucketname).get
    val summaries = bucket.objectSummaries.toList
    val files = summaries.filter(o => { name == o.getKey.split("/")(0) })
      .map(o => o.getKey )
      .sorted
    val alljsons = (0 to files.length-1).map( i => {
      val recent = files(i)
      val s3obj = s3.getObject("sparkbooks3", recent)
      val jsons = scala.io.Source.fromInputStream(s3obj.getObjectContent())
        .mkString.split("}")
        .map(json => json + "}")
      jsons.flatMap(StringToPost)
    })

    val json = alljsons.flatten.toSeq
    val rdd = sc.parallelize(json)
    rdd.foreach(println)
    rdd
  }
}
