import org.joda.time.DateTime

import Action._
import KinesisProducer._

import scala.io.StdIn.{readLine, readInt}

object CLI extends App () {

    private val post_format = "From: %s [%02d/%02d/%4d %02d::%02d]:\n%s\n\n"
    private val msg_format = "[%02d/%02d/%4d %02d::%02d] %s: %s\n"

    def run() : Unit = {
        clear()
        println("Welcome to SPARKBOOK!!")
        print("Username: ")
        val username = readLine()
        getUserInfo(username) match {
            case Some(user) => choose_action(user)
            case None => {
                clear()
                printf("%s, please complete your profile\n", username)
                print("Age: ")
                val age = readInt()
                print("City: ")
                val city = readLine()
                val user = User(username, age, city)
                sendUser(user)
                choose_action(user)
            } 
        }
        
    }

    private def choose_action(user: User) : Unit = {
        clear()
        printf("Hello %s! What would you like to do?\n", user.name)
        println("1 - Display someone's profile")
        println("2 - Display someone's posts")
        println("3 - Chat with someone")
        println("4 - Post something")
        println("5 - Compute brand occurence")
        println("6 - Exit")

        val choice = readLine("> ")
        clear()

        choice match {
            case "1" => setup_display_profile(user)
            case "2" => setup_display_posts(user)
            case "3" => setup_messaging(user)
            case "4" => post(user)
            case "5" => compute_brand(user)
            case "6" => exit()
            case x => {
                choose_action(user)
            }
        }
    }

    private def exit() = {
        closeSC()
        clear()
        println("Goodbye...") 
    }

    private def setup_display_profile(user: User) : Unit = {
        clear()
        print("Type the username of the profile you want to see: ")
        val other_username = readLine()
        display_profile(user, other_username)
        again(setup_display_profile, user, "profile")
    }
    
    private def display_profile(user: User, other_username: String) = {
        val user_info = getUserInfo(other_username)
        clear()
       
        user_info match {
            case None => println("This user doesn't exists") 
            case Some(info)  => { 
                printf("Username: %s\n", info.name)
                printf("Age: %d\n", info.age)
                printf("City: %s\n", info.city)
            }
        }
    }

    private def setup_display_posts(user: User) : Unit = {
        clear()
        print("Type the username of the person whose posts you want to see: ")
        val other_username = readLine()
        display_posts(user, other_username)
        again(setup_display_posts, user, "posts")
    }

    private def display_posts(user: User, other_username: String) = {
        val user_info = getUserInfo(other_username)
        clear()
        user_info match {
            case None => println("This user doesn't exists") 
            case Some(other_user) => {
                val posts = getPostsFromUser(other_user.name)
                posts.sortBy(_.postedOn.getMillis)
                     .collect()
                     .map(post => printf(post_format,
                                         other_username,
                                         post.postedOn.dayOfMonth().get(),
                                         post.postedOn.monthOfYear().get(),
                                         post.postedOn.year().get(),
                                         post.postedOn.hourOfDay.get(),
                                         post.postedOn.minuteOfHour().get(),
                                         post.text))
            }
        }
    }

    private def setup_messaging(user: User) : Unit = {
        print("Type the username of the person you want to message: ")
        val other_username = readLine()
        val other_user = getUserInfo(other_username)
        clear()
        other_user match {
            case None => readLine("This user doesn't exists. Press ENTER to continue.")
            case Some(other_user) => messaging(user, other_user)
        }
        choose_action(user)
    }

    private def messaging(user: User, other_user: User) {

        val msgs = getMessageFromTo(user.name, other_user.name)
        val sorted_msgs = msgs.sortBy(_.postedOn.getMillis)
                              .collect()
        clear()
        sorted_msgs.map(msg => printf(msg_format,
                                      msg.postedOn.dayOfMonth().get(),
                                      msg.postedOn.monthOfYear().get(),
                                      msg.postedOn.year().get(),
                                      msg.postedOn.hourOfDay.get(),
                                      msg.postedOn.minuteOfHour().get(),
                                      getUserInfo(msg.author).getOrElse(null).name,
                                      msg.text))
        

        val msg = readLine("> ")

        msg match {
            case "\\q" =>
            case msg => {
                val message = Message(user.name,
                                      other_user.name,
                                      DateTime.now(),
                                      msg)
                sendMessage(message)
                messaging(user, other_user)
            }
        }
    }

    private def post(user: User) = {
        clear()
        val msg = readLine("> ")
        val post = Post(user.name, DateTime.now(), msg)
        sendPost(post)
        choose_action(user)
    }

    private def compute_brand(user: User) = {
        print("Brands (separated with /): ")
        val brands = readLine().split("/")
        
        val from = parse_date("From: ")
        val to = parse_date("To: ")

        val occurences = getBrandOccurrences(from, to, brands.toList)
        clear()

        printf("There are %d occurences of ", occurences)
        brands.map(brand => printf("%s ", brand))
        printf("between %02d-%02d-%02d and %02d-%02d-%02d\n\n",
               from.dayOfMonth().get(),
               from.monthOfYear().get(),
               from.year().get(),
               to.dayOfMonth().get(),
               to.monthOfYear().get(),
               to.year().get())

        readLine("Press ENTER to continue")

        choose_action(user)
    }

    private def parse_date(msg: String) : DateTime = {
        try {
            val date = readLine(msg).split("-")
            new DateTime(date(2).toInt, date(1).toInt, date(0).toInt, 0, 0)
        } catch {
            case _ : Throwable => {
                clear()
                println("Invalid format, should be DD-MM-YYYY")
                parse_date(msg)
            }    
        }
    }

    private def again(f: User => Unit, user: User, msg: String) : Unit = {
        printf("Would you like to look at someone else's %s? (y/n)\n", msg)
        val choice = readLine("> ")
        choice match {
            case "y" => f(user)
            case "n" => choose_action(user)
            case x => {
                clear()
                printf("%s is not a valid option, please type y or n\n", x)
                again(f, user, msg)
            }
        }
    }

    private def clear() = print("\033[H\033[2J")

    run()
}
